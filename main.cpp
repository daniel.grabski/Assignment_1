/**
* Assignment no. 1
* Write a program to extract data from a file and process it according to the following algorithm:
* • filter out all characters outside the range 0 - F (hexadecimal digits)
* • all characters included in the above range should be placed in the one-byte array of items, in such a way that two characters make up one byte (3 + C -> 3C)
* • the resulting array of single-byte elements should be divided into two parts based on information whether the number of ones in a given byte is even
* • numbers containing an even number of ones should be in the output file sorted in ascending order, while the remaining numbers sorted in descending order
*/


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm> 

using namespace std;

/**
* Function conversion() convert value charakter to value in 0x00 - 0x0F range
* @param charakter character that we want to convert
* @return ret converted value
*/
unsigned char conversion(char charakter);

/**
* Function load_file() load data from a file, filters and segregates into vectors.
* @param file file that we read data
* @param vector_even vector for data storage with an even number of ones
* @param vector_odd vector for data storage with an odd number of ones
*/
void load_file(ifstream &file, vector<unsigned char> &vector_even, vector<unsigned char> &vector_odd);

/**
* Function separation() calculates how many ones is in a variable and separate to the appropriate vector.
* @param letter_s variable that we want to separete
* @param vec_even vector for data storage with an even number of ones
* @param vec_odd vector for data storage with an odd number of ones
*/

void separation(unsigned char letter_s, vector<unsigned char> &vec_even, vector<unsigned char> &vec_odd);

int main()
{
	ifstream in_file;
	ofstream out_file;
	vector<unsigned char> data_even;
	vector<unsigned char> data_odd;
	//open the input file

	string name_in_file, name_out_file;
	cout << "Name of the input file: " << endl;
	cin >> name_in_file;
	in_file.open(name_in_file.c_str());

	//checking if the file has been opened

	if (!in_file.is_open())
	{		
		cout << "Error: the file can not be opened for reading." << endl;
		system("pause");
		return -1;
	}
	//load data
	load_file(in_file, data_even, data_odd);

	//close file
	in_file.close();

	// sorting data in vectors

	sort(data_even.begin(), data_even.end());
	sort(data_odd.begin(), data_odd.end());

	//open output file

	cout << "Name of the output file: " << endl;
	cin >> name_out_file;
	out_file.open(name_out_file.c_str());

	//checking if the file has been opened

	if (!out_file.is_open())
	{
		cout << "ERROR: the file can not be opened for writing." << endl;
		system("pause");
		return -2;
	}

	// write data to file

	for(int i=0; i<data_even.size(); i++)
	{	
		out_file <<hex<< (int)data_even[i]<<" ";
		
	}
	out_file << endl;
	
	for (int i = data_odd.size(); i>0; i--)
	{
		out_file << hex << (int)data_odd[i-1] << " ";
		
	}
	cout << "Saved data to " << name_out_file << endl;
	//close file

	out_file.close();
	
	system("pause");
	return 0;
}




unsigned char conversion(char charakter)
{
	unsigned char ret;
	if (charakter >= '0' && charakter <= '9')
		ret = charakter - '0';
	else ret = charakter - 'A' + 10;
	return ret;
}

void load_file(ifstream &file, vector<unsigned char> &vector_even, vector<unsigned char> &vector_odd)
{
	char letter;
	unsigned char char_conv;
	bool is_started_byte = false;
	while (file.get(letter))
	{
		if ((letter >= '0' && letter <= '9') || (letter >= 'A' && letter <= 'F'))
		{
			if (false != is_started_byte)
			{
				char_conv = (char_conv << 4) + conversion(letter);
				is_started_byte = false;
				separation(char_conv, vector_even, vector_odd);
			}
			else
			{
				char_conv = conversion(letter);				
				is_started_byte = true;
			}
		}
	}
	if (false != is_started_byte)
	{
		separation(char_conv, vector_even, vector_odd);
	}

}

void separation(unsigned char letter_s, vector<unsigned char>& vec_even, vector<unsigned char>& vec_odd)
{
	int number_of_ones=0;
	unsigned char letter_ones = letter_s;
	while (letter_ones)
	{
		letter_ones = (letter_ones - 1)&letter_ones;
		number_of_ones++;
	}
	if (number_of_ones % 2)
	{
		vec_odd.push_back(letter_s);
	}
	else
	{
		vec_even.push_back(letter_s);
	}
}

